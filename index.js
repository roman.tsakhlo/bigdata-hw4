const cassandra = require("cassandra-driver");
const express = require("express");
const axios = require("axios");
const app = express();
const { catalog } = require("./catalog.js");
const { queryTableBook, tableBook } = require("./tables.js");
const { insertInitialCatalog } = require("./insert.js");

const PORT = process.env.PORT || 9000;
const keyspaceName = "bookrest";

const client = new cassandra.Client({
  contactPoints: ["127.0.0.1"],
  localDataCenter: "datacenter1",
  keyspace: keyspaceName,
  // keyspace: "system",
});

async function createKeySpace() {
  const queryKeyspace = `
CREATE KEYSPACE IF NOT EXISTS ${keyspaceName}
WITH replication = {'class': 'SimpleStrategy', 'replication_factor': '3'};
`;

  await client.execute(queryKeyspace, (err) => {
    if (err) {
      console.error("ERROR CREATING KEYSPACE:", err);
    } else {
      console.log(`Keyspace '${keyspaceName}' created or already exists`);
    }
  });
}

async function createTables() {
  await client.execute(queryTableBook);
  console.log("Table created successfully");
}

let counter = 0;
async function insertCatalog() {
  catalog.map((item) => {
    values = [item.title, item.author, item.genre];
    insertInitialCatalog(client, values, item.year);
  });
}

async function runCassandra() {
  try {
    await client.connect();
    // await createKeySpace();
    // await createTables();
    // await insertCatalog();
  } catch (error) {
    console.error("RUN ERROR", error);
  }
}
runCassandra();

// ----------------------------------

app.use(express.json());

app.get("/api/books", async (req, res) => {
  let result = {};
  const { author, year, genre } = req.query;

  if (author) {
    const result = await client.execute(
      `SELECT * FROM ${tableBook} WHERE author = '${author}' ALLOW FILTERING`
    );
    res.json(result.rows);
    return;
  } else if (year) {
    const result = await client.execute(
      `SELECT * FROM ${tableBook} WHERE year = ${year} ALLOW FILTERING`
    );
    res.json(result.rows);
    return;
  } else if (genre) {
    const result = await client.execute(
      `SELECT * FROM ${tableBook} WHERE genre = '${genre}' ALLOW FILTERING`
    );
    res.json(result.rows);
    return;
  } else {
    result = await client.execute(`SELECT * FROM ${tableBook}`);
    res.json(result.rows);
  }
});

app.get("/api/books/:id", async (req, res) => {
  const bookId = req.params.id;

  if (bookId) {
    const querySelectBookByBookId = `SELECT * FROM ${tableBook} WHERE book_id = ${bookId};`;
    await client.execute(querySelectBookByBookId, (err, result) => {
      if (err) {
        console.error("Error executing querySelectBookByBookId:", err);
        return res.status(404).json({ error: "Book not found" });
      } else {
        console.log("querySelectBookByBookId - Selected rows:", result.rows);
        return res.json(result.rows);
      }
    });
  }
});

app.post("/api/books", (req, res) => {
  const { title, author, year, genre } = req.body;

  if (!title || !author || !year || !genre) {
    return res.status(400).json({ error: "Please, add all required fields" });
  }

  const newBook = {
    title: title,
    author: author,
    genre: genre,
  };

  const queryInsertBook = `
  INSERT INTO ${tableBook} (
  book_id, title, author, year, genre
) VALUES (uuid(), ?, ?, ${year}, ?);
`;

  client.execute(queryInsertBook, newBook, (err, result) => {
    if (err) {
      console.error("ERROR queryInsertBook QUERY:", err);
    } else {
      console.log("queryInsertBook - Data inserted successfully", result);
      res.json({ message: "Book added successfully", book: newBook });
    }
  });
});

app.get("/api/fake-post", async (req, res) => {
  try {
    const postData = {
      title: "The best book",
      author: "I am",
      year: 2500,
      genre: "plagiarism from AI",
    };

    const response = await axios.post(
      `http://localhost:${PORT}/api/books`,
      postData
    );
    console.log("response:", response.data);
    res.json(response.data);
  } catch (error) {
    console.error("error post:", error.message);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
