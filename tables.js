const tableBook = "tableBook";

module.exports.tableBook = tableBook;
module.exports.queryTableBook = `
CREATE TABLE IF NOT EXISTS ${tableBook} (
    book_id UUID,
    title TEXT,
    author TEXT,
    year INT,
    genre TEXT,
    PRIMARY KEY (book_id, title)
);
`;
