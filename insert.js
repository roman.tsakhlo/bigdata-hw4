const { tableBook } = require("./tables");

module.exports.insertInitialCatalog = (client, values, year) => {
  const queryInsertInitialCatalog = `
INSERT INTO ${tableBook} (
    book_id, title, author, year, genre
) VALUES (uuid(), ?, ?, ${year}, ?);
`;

  client.execute(queryInsertInitialCatalog, values, (err, result) => {
    if (err) {
      console.error("ERROR insertCatalog QUERY:", err);
    } else {
      console.log("insertCatalog - Data inserted successfully", result);
    }
  });
};
