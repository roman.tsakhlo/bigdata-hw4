const { tableBook } = require("./tables");

let selectedBookById = {};

module.exports.selectBookByBookId = async (client, bookId) => {
  const querySelectBookByBookId = `SELECT * FROM ${tableBook} WHERE book_id = ${bookId};`;

  await client.execute(querySelectBookByBookId, (err, result) => {
    if (err) {
      console.error("Error executing SELECT query:", err);
    } else {
      console.log("querySelectBookByBookId - Selected rows:", result.rows);
      selectedBookById = result.rows;
    }
  });

  return selectedBookById;
};
